export const SERVER_URL = "http://localhost:8000";
export const V_AND_A_API_URL = "https://www.vam.ac.uk/api/json/museumobject";
export const IMAGE_VERY_SMALL_URL = (imageId) => `http://media.vam.ac.uk/media/thira/collection_images/${imageId.substr(0, 6)}/${imageId}_jpg_ws.jpg`;
export const IMAGE_SMALL_URL = (imageId) => `http://media.vam.ac.uk/media/thira/collection_images/${imageId.substr(0, 6)}/${imageId}_jpg_w.jpg`;
export const IMAGE_URL = (imageId) => `http://media.vam.ac.uk/media/thira/collection_images/${imageId.substr(0, 6)}/${imageId}_jpg_l.jpg`;
