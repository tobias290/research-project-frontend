import React from "react";
import WordCloudInterface from "../components/WordCloudInterface";
import NavBar from "../components/NavBar";

export default class Overview extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filter: null,
        }
    }

    /**
     * Renders the content for the app page.
     */
    render() {
        return (
            <>
                <NavBar>
                    Overview of {this.state.filter}s
                </NavBar>
                <WordCloudInterface updatedFilter={filter => this.setState({filter: filter})} />
            </>
        );
    }
}
