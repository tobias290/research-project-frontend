import React from "react";
import NavBar from "../components/NavBar";
import {V_AND_A_API_URL} from "../../constants";
import CollectionItem from "../components/CollectionItem";
import PageInfoOrLoading from "../components/PageInfoOrLoading";
import Pagination from "../components/Pagination";
import {Link} from "react-router-dom";

export default class DateExplorer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            centuryOverview: true,
            centuryViewer: false,
            decadeToView: null,
            decadeData: [],
            loading: false,
            hasGotDecadeData: false,
            limit: 45,
            offset: 0,
            numberOfResults: null,
        };

        this.range = this.range.bind(this);
        this.viewCentury = this.viewCentury.bind(this);
        this.getCenturyData = this.getCenturyData.bind(this);
        this.updateOffset = this.updateOffset.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params.hasOwnProperty("century"))
            this.viewCentury(this.props.match.params.century);
    }

    /**
     * Called the the component state is changed.
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        // Check if we have stayed on the same page but added a route parameter
        // If so the we are viewing a specific century
        // Else we are over viewing all centuries
        if (prevProps.match.params !== this.props.match.params) {
            if (this.props.match.params.hasOwnProperty("century"))
                this.viewCentury(this.props.match.params.century);
            else
                this.setState({
                    centuryOverview: true,
                    centuryViewer: false,
                    decadeToView: null,
                    decadeData: [],
                });
        }

        // If we are viewing a century and haven't got the decade data, get it
        if (this.state.centuryViewer && !this.state.hasGotDecadeData) {
            this.getCenturyData(this.state.decadeToView);
            this.setState({hasGotDecadeData: true})
        }

        // If the offset has changed get the new offset data
        if (prevState.offset !== this.state.offset) {
            this.getCenturyData(this.state.decadeToView);
        }
    }

    /**
     * Creates an array of numbers.
     *
     * @param {number} start - The first number of the array.
     * @param {number} end - The last number of the array.
     * @param {number} step - The increment to go by between the first and last number.
     * @returns {Generator<*, void, ?>}
     */
    *range(start, end, step=1) {
        for (let i = start; i <= end; i = i + step) {
            yield i;
        }
    }

    /**
     * Moves from the century overview to century view.
     */
    viewCentury(centuryStart) {
        this.setState({
            centuryOverview: false,
            centuryViewer: true,
            decadeToView: centuryStart,
            loading: true,
        });
    }

    /**
     * Gets the data for a specific century.
     *
     * @param century
     */
    getCenturyData(century) {
       fetch(`${V_AND_A_API_URL}/search?after=${century}&before=${century + 100}&images=1&limit=${this.state.limit}&offset=${this.state.offset}`)
           .then(resp => resp.json())
           .then(resp => {
                this.setState({
                    decadeData: resp.records,
                    numberOfResults: resp.meta.result_count,
                    loading: false,
                });
               window.scrollTo(0, 0);
           })
           .catch(err => console.error(err));
    }

    /**
     * Updates the search offset.
     *
     * @param {number} offset - New offset.
     */
    updateOffset(offset) {
        this.setState({offset: offset});
    }

    /**
     * Gets the date unit
     * @param century
     * @returns {string}
     */
    getDateUnit(century) {
        return century < 0 ? "BC" : "AD";
    }

    /**
     * Renders the dates page.
     */
    render() {
        return (
            <div className="container">
                <NavBar>
                    {this.state.decadeToView === null ? "Explore by Date" : `Exploring the ${this.state.decadeToView}s (${this.getDateUnit(this.state.decadeToView)})`}
                </NavBar>

                {this.state.loading && <PageInfoOrLoading/>}

                {!this.state.loading && this.state.centuryOverview && <div className="century-overview">
                    {[...this.range(-3400, 2000, 100)].map((century, i) =>
                        <Link
                            key={i}
                            to={`/date/${century}`}
                            className="century-overview__century"
                            title={`View items from ${century}${this.getDateUnit(century)} to ${century + 100}${this.getDateUnit(century)}`}
                        >{century}{this.getDateUnit(century)}</Link>
                    )}
                </div>}

                {
                    !this.state.loading && this.state.centuryViewer &&
                    <div className="century-explorer">
                        <div className="date-explorer__century-viewer">
                            {this.state.decadeData.map((decadeDatum, i) =>
                                <CollectionItem key={i} item={decadeDatum.fields} />
                            )}
                        </div>

                        <Pagination
                            limit={this.state.limit}
                            offset={this.state.offset}
                            numberOfResults={this.state.numberOfResults}
                            updateOffset={this.updateOffset}
                        />
                    </div>
                }
            </div>
        );
    }
}
