import React from "react";
import {IMAGE_URL, IMAGE_VERY_SMALL_URL, SERVER_URL, V_AND_A_API_URL} from "../../constants";
import NavBar from "../components/NavBar";
import {Link} from "react-router-dom";
import PageInfoOrLoading from "../components/PageInfoOrLoading";


export default class Details extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            details: null,
            related: [],
        };

        this.isFavourite = this.isFavourite.bind(this);
        this.toggleFavourite = this.toggleFavourite.bind(this);
    }

    /**
     * Called when the component is loaded/reloaded.
     */
    componentDidMount() {
        this.setState({loading: true});

        fetch(`${V_AND_A_API_URL}/${this.props.match.params.objectId}`)
            .then(resp => resp.json())
            .then(resp => {
                this.setState({details: resp[0].fields});
                this.getRelatedImages(resp[0].fields.object_number);
            })
            .catch(err => console.error(err));
    }

    /**
     * Called when the state/props changes.
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        // If a new object is being views update the content and scroll to top
        if (this.props.match.params.objectId !== prevProps.match.params.objectId) {
            this.componentDidMount();
            window.scrollTo(0, 0);
        }
    }

    /**
     * Takes the details of the current image and creates a collection of related images.
     *
     * @param {string} objectNumber - Gets images relating to the current item.
     */
    getRelatedImages(objectNumber) {
        fetch(`${SERVER_URL}/get-related/${objectNumber}`)
            .then(resp => resp.json())
            .then(resp => {
                this.setState({related: resp, loading: false});
            })
            .catch(err => console.error(err));
    }

    /**
     * Converts snake case object key to title case.
     * @param {string} key - Key to convert.
     * @returns {string} - Returns the titled key.
     */
    convertKeyToTitle(key) {
        // Converts "detail-key" to "Detail Key"
        return (
            key
                .split("_")
                .map(key => key.charAt(0).toUpperCase() + key.substr(1))
                .join(" ")
        );

    }

    /**
     * @returns {boolean} - Returns true if this item is marked as a favourite.
     */
    isFavourite() {
        return (
            localStorage.hasOwnProperty("favourites") ?
            JSON.parse(localStorage.getItem("favourites")).includes(this.state.details.object_number) :
            false
        );
    }

    /**
     * Toggle the favourite state of the item.
     */
    toggleFavourite() {
        let objectId = this.state.details.object_number;

        // Check if the local storage already is saving favourites
        if (localStorage.hasOwnProperty("favourites")) {
            let favourites = JSON.parse(localStorage.getItem("favourites"));

            // If the saved data already has the item then remove it
            // Else save it
            if (favourites.includes(objectId))
                favourites.splice(favourites.indexOf(objectId), 1);
            else
                favourites.push(objectId);

            // Update storage
            localStorage.setItem("favourites", JSON.stringify(favourites));
        } else {
            // No data has been saved therefore save the new item
            localStorage.setItem("favourites", JSON.stringify([objectId]));
        }

        // Update the page so the floating button state is changed
        this.forceUpdate();
    }

    /**
     * Render details page.
     */
    render() {
        return this.state.loading ? <PageInfoOrLoading /> : this.renderLoaded();
    }

    /**
     * Render loaded details page.
     */
    renderLoaded() {
        let details = this.state.details;

        return (
            <>
                <NavBar>
                    {details.title === "" ? "No Title" : details.title}&nbsp;
                    <strong>(ID: {details.object_number})</strong>
                </NavBar>

                <div className="details">
                    <div className="details__image">
                        <img
                            src={IMAGE_URL(String(details.primary_image_id))}
                            alt={details.title}
                            title={details.title}
                        />
                    </div>
                    <div className="details__info">
                        {/* Loop over all the details within an object and display its information */}
                        {Object.entries(details).filter(([_, value]) => value !== "" && typeof value !== "object").map(([key, value]) =>
                            this.renderDetail(key, value)
                        )}
                    </div>
                </div>

                {
                    this.state.related.artist !== null &&
                    <>
                        <h1 className="related-title">Related By Artist</h1>
                        <div className="related">
                            {/* Loop over each related record and display its image */}
                            {this.state.related.artist.length !== 0 && this.state.related.artist.map((result, i) =>
                                <Link key={i} className="related__image" to={`/details/${result.fields.object_number}`}>
                                    <img
                                        src={IMAGE_VERY_SMALL_URL(String(result.fields.primary_image_id))}
                                        alt={result.fields.title}
                                        title={result.fields.title + " (Click for details)"}
                                    />
                                </Link>
                            )}
                        </div>
                    </>
                }

                {
                    this.state.related.object !== null &&
                    <>
                        <h1 className="related-title">Related By Object</h1>
                        <div className="related">
                            {/* Loop over each related record and display its image */}
                            {this.state.related.object.length !== 0 && this.state.related.object.map((result, i) =>
                                <Link key={i} className="related__image" to={`/details/${result.fields.object_number}`}>
                                    <img
                                        src={IMAGE_VERY_SMALL_URL(String(result.fields.primary_image_id))}
                                        alt={result.fields.title}
                                        title={result.fields.title + " (Click for details)"}
                                    />
                                </Link>
                            )}
                        </div>
                    </>
                }

                {
                    this.state.related.place !== null &&
                    <>
                        <h1 className="related-title">Related By Place</h1>
                        <div className="related">
                            {/* Loop over each related record and display its image */}
                            {this.state.related.place.length !== 0 && this.state.related.place.map((result, i) =>
                                <Link key={i} className="related__image" to={`/details/${result.fields.object_number}`}>
                                    <img
                                        src={IMAGE_VERY_SMALL_URL(String(result.fields.primary_image_id))}
                                        alt={result.fields.title}
                                        title={result.fields.title + " (Click for details)"}
                                    />
                                </Link>
                            )}
                        </div>
                    </>
                }

                {
                    this.state.related.material !== null &&
                    <>
                        <h1 className="related-title">Related By Material Techniques</h1>
                        <div className="related">
                            {/* Loop over each related record and display its image */}
                            {this.state.related.material.length !== 0 && this.state.related.material.map((result, i) =>
                                <Link key={i} className="related__image" to={`/details/${result.fields.object_number}`}>
                                    <img
                                        src={IMAGE_VERY_SMALL_URL(String(result.fields.primary_image_id))}
                                        alt={result.fields.title}
                                        title={result.fields.title + " (Click for details)"}
                                    />
                                </Link>
                            )}
                        </div>
                    </>
                }

                <button
                    className="button button--floating"
                    title={this.isFavourite() ? "Remove as Favourite" : "Mark as Favourite"}
                    onClick={this.toggleFavourite}
                >
                    <i className={`${this.isFavourite() ? "fas" : "far"} fa-heart`} />
                </button>
            </>
        );
    }

    /**
     * Renders a single detail about the item
     *
     * @param {string} key - Name of the detail.
     * @param {string} value - Actual value of the detail.
     */
    renderDetail(key, value) {
        return (
            <div className="details__detail" key={key}>
                <div className="details__detail-key">{this.convertKeyToTitle(key)}</div>
                <div  className="details__detail-value">
                    {
                        key !== "materials_techniques" && key !== "place"  && key !== "object" && key !== "artist" &&
                        <span dangerouslySetInnerHTML={{ __html: value}} />
                    }
                    {key === "artist" && <Link to={`/explore/artist/${value}`}>{value}</Link>}
                    {key === "object" && <Link to={`/explore/object/${value}`}>{value}</Link>}
                    {key === "place" && <Link to={`/explore/place/${value}`}>{value}</Link>}
                    {key === "materials_techniques" && <Link to={`/explore/material/${value}`}>{value}</Link>}
                </div>
            </div>
        );
    }
}
