import React from "react";
import {Link} from "react-router-dom";


const Index = () => (
    <div id="container">
        <h1 id="jumbotron">
            Digital Archives
            <br/>
            Collections Explorer
        </h1>

        <Link id="get-viewing" className="button" to="/overview">
            View Collections <i className="fas fa-arrow-right" />
        </Link>
    </div>
);

export default Index;
