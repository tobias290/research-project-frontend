import React from "react";
import {SERVER_URL, V_AND_A_API_URL} from "../../constants";
import NavBar from "../components/NavBar";
import PageInfoOrLoading from "../components/PageInfoOrLoading";


export default class Map extends React.Component {
    constructor(props) {
        super(props);

        this.state =  {
            loading: true,
        }
    }

    /**
     * Called the the component is loaded/reloaded.
     */
    componentDidMount() {
        // Create the map
        Map.map = window.L.map("map").setView([0, 0], 2);

        // Add the map layer
        // Add the base settings
        window.L.tileLayer(`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}`, {
            attribution: "Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>",
            maxZoom: 18,
            minZoom: 2,
            id: "mapbox/streets-v11",
            tileSize: 512,
            zoomOffset: -1,
            accessToken: "pk.eyJ1IjoidG9iaWFzMjkwIiwiYSI6ImNrN2JyZTYwODAyY20zbG4wejRuZnFpcWgifQ.i6BCF2b1b8mVP6VOnSHN4w"
        }).addTo(Map.map);

        // Takes the country shapes data and converts it into GeoJSON data
        fetch(`${SERVER_URL}/map-data`)
            .then(resp => resp.json())
            .then(data => {
                this.setState({loading: false});

                let geoData = {
                    type: "FeatureCollection",
                    bbox: [
                        -179.99999999999997,
                        -90,
                        180,
                        83.6235960000000,
                    ],
                    features: [],
                };

                // Get each country that has items
                fetch(`${SERVER_URL}/get-word-count/country`)
                    .then(resp => resp.json())
                    .then(resp => {
                        let placeKeys = Object.keys(resp);

                        // Loop over each country
                        for (let placeIndex in placeKeys) {
                            let place = Object.keys(resp)[placeIndex];

                            // Get the country's map data from the list of features
                            // This includes the country outline poly-shape data
                            let feature = data.features.find(feature => feature.properties.NAME === place);

                            // Only continue if the feature exists.
                            if (feature !== undefined) {
                                // Fetch the result count for the specific country
                                fetch(`${V_AND_A_API_URL}/search?placesearch=${place}`)
                                    .then(resp => resp.json())
                                    .then(resp => {
                                        // Add the density to the feature
                                        feature["properties"]["density"] = resp.meta.result_count;
                                        // Add the feature to teh geo data list
                                        geoData.features.push(feature);

                                        // If this is the last country add the data to the map
                                        if (parseInt(placeIndex) + 1 === placeKeys.length)
                                            Map.geoData = window.L.geoJson(geoData, {
                                                style: Map.style,
                                                onEachFeature: Map.onEachFeature
                                            }).addTo(Map.map);
                                    })
                                    .catch(err => console.log(err));
                            }
                        }
                    })
                    .catch(err => console.error(err));
        })
            .catch(err => console.log(err));

        // Create the information panel control
        Map.info = window.L.control();
        Map.info.onAdd = Map.infoOnAdd;
        Map.info.update = Map.infoUpdate;
        Map.info.addTo(Map.map);

        // Create the legend panel
        Map.legend = window.L.control({position: "bottomright"});
        Map.legend.onAdd = Map.legendOnAdd;
        Map.legend.addTo(Map.map);
    }

    /**
     * Called when the info control is added to the map.
     *
     * @returns {div} - Return the info HTML element.
     */
    static infoOnAdd()  {
        this._div = window.L.DomUtil.create("div", "info"); // create a div with a class "info"
        this.update();
        return this._div;
    }

    /**
     * Called when we want to update the info control.
     *
     * @param {object} props - Properties of the area of map hovered over.
     * @param {number} positionX - X co-ordinates of the mouse.
     * @param {number} positionY - Y co-ordinates of the mouse.
     */
    static infoUpdate(props, positionX, positionY) {
        // If the user is hovered over a country position the info panel to be where the user's mouse it
        // Else we are resetting the view
        if (positionX !== undefined && positionY !== undefined) {
            this._div.style.position = "absolute";
            this._div.style.left = positionX + "px";
            this._div.style.top = positionY - 200 + "px";
        } else {
            this._div.style.position = "static";
        }

        this._div.innerHTML = `
            <h4>What's available here?</h4>
            ${props ?
            `<b>${props.NAME}</b>
            <br /> 
            ${props.density} Items Located Here 
            <br /> 
            <i>(Click to explore this place's collection)</i>`:
            "Hover over a country"}
        `;
    }

    /**
     * Called when the legend control is added to the map.
     *
     * @returns {div} - Return the legend HTML element.
     */
    static legendOnAdd() {
        let div = window.L.DomUtil.create("div", "info legend");
        let grades = [0, 100, 200, 500, 1000, 5000, 10000, 1000000];

        // Loop through the density values and generate a label with a coloured square for each interval
        for (let i = 0; i < grades.length; i++) {
            div.innerHTML +=
                "<span><i style=\"background:" + Map.getColor(grades[i] + 1) + "\"></i> " +
                grades[i] + (grades[i + 1] ? " &ndash; " + grades[i + 1] + " Items</span><br>" : "+ Items");
        }

        return div;
    }

    /**
     * Returns the styles of the map
     *
     * @param {object} feature - Feature object to determine the color of the country.
     */
    static style(feature) {
        return {
            fillColor: Map.getColor(feature.properties.density),
            weight: 2,
            opacity: 1,
            color: "white",
            dashArray: "3",
            fillOpacity: 0.7
        };
    }

    /**
     * Returns the right color the the area of map/density
     *
     * @param {number} d - Density of the country.
     * @returns {string} - Returns the color related to the density.
     */
    static getColor(d) {
        return d > 100000 ? "#800026" :
            d > 10000 ? "#BD0026" :
                d > 5000 ? "#E31A1C" :
                    d > 1000 ? "#FC4E2A" :
                        d > 500 ? "#FD8D3C" :
                            d > 200 ? "#FEB24C" :
                                d > 100 ? "#FED976" :
                                    "#FFEDA0";
    }

    /**
     * Called then the user hovers over a country.
     * Created a #666 border around the country.
     *
     * @param e - Event (used to get the country).
     */
    static highlightFeature(e) {
        // Get the country
        let layer = e.target;

        // Update the country's/layer's styling
        layer.setStyle({
            weight: 2,
            color: "#666",
            dashArray: "",
            fillOpacity: 0.7
        });

        // Bring the layer to the front
        if (!window.L.Browser.ie && !window.L.Browser.opera && !window.L.Browser.edge)
            layer.bringToFront();

        // Update the information
        Map.info.update(layer.feature.properties, e.originalEvent.clientX, e.originalEvent.clientY);
    }

    /**
     * Resets the country highlighting back to normal.
     *
     * @param e - Event (used to get the country).
     */
    static resetHighlight(e) {
        Map.geoData.resetStyle(e.target);
        Map.info.update();
    }

    /**
     * When the user clicks on a country it takes them to the explore page for that country.
     *
     * @param e - Event (used to get the country).
     */
    static exploreCountry(e) {
        // Get the countries name
        let country = e.target.feature.properties.NAME;

        // Load the explore page for this country
        window.location.href = `${window.location.origin}/#/explore/place/${country}`;
    }

    /**
     * Adds the events for each country.
     *
     * @param feature
     * @param layer
     */
    static onEachFeature(feature, layer) {
        layer.on({
            mouseover: Map.highlightFeature,
            mouseout: Map.resetHighlight,
            click: Map.exploreCountry
        });
    }

    /**
     * Renders the map.
     */
    render() {
        return (
            <>
                <NavBar>Explore the Collections with Maps</NavBar>
                <div id="map"/>
                {this.state.loading && <PageInfoOrLoading />}
            </>
        );
    }
}
