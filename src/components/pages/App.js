import React from "react";
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import Index from "./Index";
import Explore from "./Explore";
import Search from "./Search";
import Details from "./Details";
import Overview from "./Overview";
import Favourites from "./Favourites";
import Map from "./Map";
import DateExplorer from "./DateExplorer";
import "../../css/styles.css";


const App = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Index}/>
            <Route path="/overview" component={Overview}/>
            <Route path="/explore/:type/:item" component={Explore}/>
            <Route path="/details/:objectId" component={Details}/>
            <Route path="/favourites" component={Favourites}/>
            <Route path="/search" component={Search}/>
            <Route path="/map" component={Map}/>
            <Route exact path="/date" component={DateExplorer}/>
            <Route path="/date/:century" component={DateExplorer}/>
        </Switch>
    </Router>
);

export default App;
