import React from "react";
import {V_AND_A_API_URL} from "../../constants";
import NavBar from "../components/NavBar";
import PageInfoOrLoading from "../components/PageInfoOrLoading";
import CollectionItem from "../components/CollectionItem";

export default class Favourites extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            favourites: [],
        }
    }

    /**
     * Called when the component is loaded/reloads.
     */
    componentDidMount() {
        // Get the favourites from local storage
        let favourites = localStorage.hasOwnProperty("favourites") ? JSON.parse(localStorage.getItem("favourites")) : [];

        // If the saved data is empty then stop loading and return
        // Therefore the page will inform the user no items have been saved
        if (favourites.length === 0) {
            this.setState({loading: false});
            return;
        }

        // Get each item's data saved
        for (let index in favourites)
            fetch(`${V_AND_A_API_URL}/${favourites[index]}`)
                .then(resp => resp.json())
                .then(resp => {
                    this.setState({
                        favourites: [...this.state.favourites, resp[0]["fields"]],
                        loading: index - 1 === favourites.length
                    })
                })
                .catch(err => console.error(err));
    }

    /**
     * Renders the favourites page.
     */
    render() {
        return (
            <>
                <NavBar>
                    Favourites
                </NavBar>

                {this.state.loading && <PageInfoOrLoading />}
                {!this.state.loading && this.state.favourites.length === 0 && <PageInfoOrLoading>No Favourites</PageInfoOrLoading>}
                <div className="favourites">
                    {!this.state.loading && this.state.favourites.length !== 0 && this.state.favourites.map((favourite, i) =>
                        <CollectionItem key={i} item={favourite} />
                    )}
                </div>
            </>
        );
    }
}
