import React from "react";
import {V_AND_A_API_URL} from "../../constants";
import NavBar from "../components/NavBar";
import CollectionItem from "../components/CollectionItem";
import Pagination from "../components/Pagination";
import PageInfoOrLoading from "../components/PageInfoOrLoading";
import WordCloudInterface from "../components/WordCloudInterface";

export default class Explore extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            results: null,
            numberOfResults: null,
            limit: 45,
            offset: 0,
        };

        this.updateOffset = this.updateOffset.bind(this);
    }

    /**
     * Called first time component is loaded or when page is reloaded.
     */
    componentDidMount() {
        this.setState({loading: true});

        // Get offset (possible from storage)
        let offset;

        // Check if session storage has saved an offset
        // If true do more checks
        // Else set the offset to the state's offset
        if (sessionStorage.hasOwnProperty("offset")) {
            // Get saved offset
            let offsetObject = JSON.parse(sessionStorage.getItem("offset"));

            // If an offset has been saved check we are still searching in the same item
            // If so keep the saved offset
            // Else restart because we are now searching something new
            if (offsetObject.search === this.props.match.params.item) {
                offset = offsetObject.offset;

                // Only if a new offset is found update the state
                if (parseInt(offset) !== parseInt(this.state.offset))
                    this.setState({offset: offset});
            } else {
                offset = this.state.offset;
            }
        } else {
            offset = this.state.offset;
        }

        let searchType;

        // Get the right search type
        switch (this.props.match.params.type) {
            case "artist":
            default:
                searchType = "namesearch";
                break;
            case "place":
                searchType = "placesearch";
                break;
            case "object":
                searchType = "objectnamesearch";
                break;
            case "material":
                searchType = "materialsearch";

        }

        let url = `${V_AND_A_API_URL}/search?${searchType}=${this.props.match.params.item}&limit=${this.state.limit}&offset=${offset}&images=1`;

        fetch(url)
            .then(resp => resp.json())
            .then(resp => {
                this.setState({
                    results: resp.records,
                    numberOfResults: resp.meta.result_count,
                    loading: false,
                });
                window.scrollTo(0, 0);
            })
            .catch(err => console.error(err));
    }

    /**
     * Called when a state change is made.
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        // If the offset has changed then we want to load a new set of results (therefore call 'componentDidMount()')
        if (prevState.offset !== this.state.offset) {
            sessionStorage.setItem("offset", JSON.stringify({search: this.props.match.params.item, offset: this.state.offset}));
            this.componentDidMount();
        }
    }

    /**
     * Updates the search offset.
     *
     * @param {number} offset - New offset.
     */
    updateOffset(offset) {
        this.setState({offset: offset});
    }

    /**
     * Render explore page.
     */
    render() {
        return this.state.loading ? <PageInfoOrLoading /> : this.renderLoaded();
    }

    /**
     * Render loaded explore page.
     */
    renderLoaded() {
        return (
            <>
                <NavBar>
                    <span>Exploring {this.props.match.params.type}</span>&nbsp;<strong>{this.props.match.params.item}</strong>
                </NavBar>

                {this.state.results.length === 0 && <PageInfoOrLoading>No Results</PageInfoOrLoading>}

                {
                    this.state.results.length !== 0 &&
                    <>
                        <div className="image-tile">
                            {/* Render the results as a grid of image tiles  */}
                            {this.state.results.map((result, i) =>
                                <CollectionItem key={i} item={result.fields} />
                            )}
                        </div>

                        <Pagination
                            limit={this.state.limit}
                            offset={this.state.offset}
                            numberOfResults={this.state.numberOfResults}
                            updateOffset={this.updateOffset}
                        />
                    </>
                }
            </>
        );
    }
}
