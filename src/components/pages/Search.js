import React from "react";
import NavBar from "../components/NavBar";
import CollectionItem from "../components/CollectionItem";
import {V_AND_A_API_URL} from "../../constants";
import Pagination from "../components/Pagination";
import PageInfoOrLoading from "../components/PageInfoOrLoading";

export default class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            hasSearched: false,
            searchQuery: null,
            searchResults: [],
            limit: 45,
            offset: 0,
            numberOfResults: null,
        };

        this.onSearch = this.onSearch.bind(this);
        this.search = this.search.bind(this);
        this.updateOffset = this.updateOffset.bind(this);
    }

    /**
     * Called when a state change is made.
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        // If the offset has changed then we want to load a new set of results (therefore call 'componentDidMount()')
        if (prevState.offset !== this.state.offset) {
            window.scrollTo(0, 0);
            this.search(this.state.searchQuery);
        }
    }

    /**
     * Updates the search history saves in session storage.
     *
     * @param {string} query - Search query.
     */
    updateSearchHistory(query) {
        // Only run if there is a search query
        if (query !== "") {
            // Check if history is already being saved
            if (sessionStorage.hasOwnProperty("searchHistory")) {
                let history = JSON.parse(sessionStorage.getItem("searchHistory"));

                // If the history already has the query then don't add it
                if (!history.includes(query)) {
                    history.push(query);
                    sessionStorage.setItem("searchHistory", JSON.stringify(history))
                }
            } else {
                sessionStorage.setItem("searchHistory", JSON.stringify([query]));
            }
        }
    }


    /**
     * Called when the enter button is clicked in the search bar.
     */
    onSearch(event) {
        // Only search if the user clicks the enter key while focused of the search bar
        if (event.keyCode === 13) {
            this.setState({loading: true, searchQuery: event.target.value});

            // Only search if there is a search query value
            if (event.target.value !== "") {
                this.updateSearchHistory(event.target.value);
                this.search(event.target.value);
            } else
                this.setState({searchResults: [], loading: false, hasSearched: true});
        }
    }

    /**
     * Searched with the given query and display the given results.
     * @param {string} query - Search query.
     */
    search(query) {
        fetch(`${V_AND_A_API_URL}/search?q=${query}&images=1&limit=${this.state.limit}&offset=${this.state.offset}`)
            .then(resp => resp.json())
            .then(resp => {
                this.setState({
                    searchResults: resp.records,
                    loading: false,
                    hasSearched: true,
                    numberOfResults: resp.meta.result_count,
                });
            })
            .catch(err => console.error(err));
    }

    /**
     * Updates the search offset.
     *
     * @param {number} offset - New offset.
     */
    updateOffset(offset) {
        this.setState({offset: offset});
    }

    render() {
        return (
            <>
                <NavBar>
                    <span className="search-box">
                        <i className="fas fa-search search-icon"/>
                        <input className="search" type="search" placeholder="Search" onKeyUp={this.onSearch} list="history" />
                        {/* Create a datalist based of the saved search history */}
                        {sessionStorage.hasOwnProperty("searchHistory") && <datalist id="history">
                            {JSON.parse(sessionStorage.getItem("searchHistory")).map(item =>
                                <option value={item} />
                            )}
                        </datalist>}
                    </span>
                </NavBar>

                {this.state.loading && <PageInfoOrLoading/>}
                {
                    !this.state.loading && (!this.state.hasSearched || this.state.searchResults.length === 0) &&
                    <PageInfoOrLoading>
                        {this.state.hasSearched ? "No Results" : "Search to explore the collection..."}
                    </PageInfoOrLoading>
                }

                {
                    this.state.searchResults.length !== 0 &&
                    <>
                        <div className="search-results">
                            {this.state.searchResults.map((searchResult, i) =>
                                <CollectionItem key={i} item={searchResult.fields}/>
                            )}
                        </div>

                        <Pagination
                            limit={this.state.limit}
                            offset={this.state.offset}
                            numberOfResults={this.state.numberOfResults}
                            updateOffset={this.updateOffset}
                        />
                    </>
                }
            </>
        );
    }
}
