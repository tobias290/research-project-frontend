import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {SERVER_URL} from "../../constants";
import PageInfoOrLoading from "./PageInfoOrLoading";

export default class WordCloudInterface extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            filter: "artist",
            displayAsList: false,
            words: {},
        };
    }

    /**
     * Called when the component mounts for the first time or when the page is reloaded.
     */
    componentDidMount() {
        let filter = sessionStorage.hasOwnProperty("filter") ? sessionStorage.getItem("filter") : "artist";

        this.setState({
            filter: sessionStorage.hasOwnProperty("filter") ? sessionStorage.getItem("filter") : "artist"
        });
        this.props.updatedFilter(filter);

        this.componentDidUpdate(null, {filter: null}, null);
    }

    /**
     * Called when the component state is updated
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        // If the props have changed update the state and get the new words for the word cloud
        if (this.state.filter !== prevState.filter) {
            sessionStorage.setItem("filter", this.state.filter);

            fetch(`${SERVER_URL}/get-word-count/${this.state.filter}`)
                .then(resp => resp.json())
                .then(resp => {
                    this.setState({words: resp, loading: false})
                })
                .catch(err => console.error(err));
        }
    }

    /**
     * Updates the word cloud interface filter type.
     * From either artist, place or object type.
     *
     * @param {string} filter - Filter type.
     */
    updateWordCloud(filter) {
        this.setState({filter: filter, loading: true});
        this.props.updatedFilter(filter);
    }

    /**
     * Renders the word cloud.
     */
    render() {
        return this.state.loading ? <PageInfoOrLoading /> : this.renderLoaded();
    }

    /**
     * Renders the loaded content
     */
    renderLoaded() {
        return (
            <>
                <div className="options">
                    <label className="form__checkbox">Display As List
                        <input type="checkbox" onClick={() => this.setState({displayAsList: !this.state.displayAsList})} />
                        <span className="form__checkbox__check" />
                    </label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <button
                        className={`button ${this.state.filter === "artist" ? "button--selected" : ""}`}
                        onClick={() => this.updateWordCloud("artist")}
                    >Artist</button>
                    <button
                        className={`button ${this.state.filter === "object" ? "button--selected" : ""}`}
                        onClick={() => this.updateWordCloud("object")}
                    >Object</button>
                    <button
                        className={`button ${this.state.filter === "place" ? "button--selected" : ""}`}
                        onClick={() => this.updateWordCloud("place")}
                    >Place</button>
                </div>
                <div className={`word-cloud ${this.state.displayAsList ? "word-cloud--as-list": ""}`}>
                    {
                        !this.state.loading &&
                        Object.entries(this.state.words).filter(([key, value]) => value > 2).map(([key, value], i) =>
                            <Link key={i} className="word-cloud__word" style={{fontSize: `${Math.log(value) * 8}px`}} to={`/explore/${this.state.filter}/${key}`}>
                                {key}{this.props.showWordCount ? `(${value})` : ""},
                            </Link>
                        )
                    }
                </div>
            </>
        );
    }
}

WordCloudInterface.propTypes = {
    showWordCount: PropTypes.bool,
    updatedFilter: PropTypes.func,
};

WordCloudInterface.defaultProps = {
    showWordCount: false,
    updatedFilter: (filter) => {},
};
