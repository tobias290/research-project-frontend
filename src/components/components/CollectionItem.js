import React from "react";
import PropTypes from "prop-types";
import {IMAGE_SMALL_URL} from "../../constants";
import {Link} from "react-router-dom";


export default class CollectionItem extends React.Component {
    constructor(props) {
        super(props);

        this.isFavourite = this.isFavourite.bind(this);
        this.toggleFavourite = this.toggleFavourite.bind(this);
    }

    /**
     * @returns {boolean} - Returns true if this item is marked as a favourite.
     */
    isFavourite() {
        return (
            localStorage.hasOwnProperty("favourites") ?
            JSON.parse(localStorage.getItem("favourites")).includes(this.props.item.object_number) :
            false
        );
    }

    /**
     * Toggle the favourite state of the item.
     */
    toggleFavourite(e) {
        e.preventDefault();

        let objectId = this.props.item.object_number;

        // Check if the local storage already is saving favourites
        if (localStorage.hasOwnProperty("favourites")) {
            let favourites = JSON.parse(localStorage.getItem("favourites"));

            // If the saved data already has the item then remove it
            // Else save it
            if (favourites.includes(objectId))
                favourites.splice(favourites.indexOf(objectId), 1);
            else
                favourites.push(objectId);

            // Update storage
            localStorage.setItem("favourites", JSON.stringify(favourites));
        } else {
            // No data has been saved therefore save the new item
            localStorage.setItem("favourites", JSON.stringify([objectId]));
        }

        // Update the page so the floating button state is changed
        this.forceUpdate();
    }

    render() {
        return (
            <Link className="collection-item" to={`/details/${this.props.item.object_number}`}>
                <img
                    src={IMAGE_SMALL_URL(String(this.props.item.primary_image_id))}
                    alt={this.props.item.title}
                    title={this.props.item.title !== "" ? this.props.item.title : "No Title" + " (Click for details)"}
                />

                <div className="collection-item__hover">
                    {this.props.item.title !== "" ? this.props.item.title : "No Title"}

                    <i
                        className={`${this.isFavourite() ? "fas" : "far"} fa-heart collection-item__favourite`}
                        title={this.isFavourite() ? "Remove as Favourite" : "Mark as Favourite"}
                        onClick={this.toggleFavourite}
                    />
                </div>
            </Link>
        );
    }
}

CollectionItem.propsTypes = {
    item: PropTypes.object,
};

