import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

class NavBar extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * Called when the nav bar is loaded.
     */
    componentDidMount() {
        document.body.onscroll = this.onScroll;
    }

    /**
     * Called when scroll is detected.
     */
    onScroll() {
        // Get the nav bar
        let navBar = document.getElementsByClassName("nav-bar")[0];

        // Sometimes the content hasn't loaded therefore the nav bar will not exist
        // If so return and wait until content has loaded
        if (navBar === undefined)
            return;

        // If the user as scrolled down more than 10 pixels add shadow to nav bar
        // If less than 10 pixels then remove it
        if (window.scrollY > 10)
            navBar.classList.add("nav-bar--scrolled");
        else
            navBar.classList.remove("nav-bar--scrolled")
    }

    /**
     * Renders the nav bar.
     */
    render() {
        return (
            <div className="nav-bar">
                {
                    this.props.backButton &&
                    <button className="nav-bar__icon" title="Back" onClick={() => window.history.back()}>
                        <i className="fas fa-arrow-left"/>
                    </button>
                }
                <span className="nav-bar__title">{this.props.children}</span>

                <div className="nav-bar__right-options">
                    <Link
                        to="/overview"
                        className={`nav-bar__icon ${window.location.pathname === "/overview" ? "nav-bar__icon--active" : ""}`}
                        title="Overview"
                        style={{textDecoration: "none"}}
                    >
                        <i className="fas fa-home" />
                    </Link>

                    <Link
                        to="/date"
                        className={`nav-bar__icon ${window.location.pathname.includes("date") ? "nav-bar__icon--active" : ""}`}
                        title="Explore Collections via Dates"
                        style={{textDecoration: "none"}}
                    >
                        <i className="fas fa-calendar-alt" />
                    </Link>

                    <Link
                        to="/map"
                        className={`nav-bar__icon ${window.location.pathname === "/map" ? "nav-bar__icon--active" : ""}`}
                        title="Explore Collections via Map"
                        style={{textDecoration: "none"}}
                    >
                        <i className="fas fa-map" />
                    </Link>

                    <Link
                        to="/search"
                        className={`nav-bar__icon ${window.location.pathname === "/search" ? "nav-bar__icon--active" : ""}`}
                        title="Search"
                        style={{textDecoration: "none"}}
                    >
                        <i className="fas fa-search" />
                    </Link>

                    <Link
                        to="/favourites"
                        className={`nav-bar__icon ${window.location.pathname === "/favourites" ? "nav-bar__icon--active" : ""}`}
                        title="Favourites"
                        style={{textDecoration: "none"}}
                    >
                        <i className="fas fa-heart" />
                    </Link>
                </div>
            </div>
        );
    }
}

NavBar.propTypes = {
    backButton: PropTypes.bool,
};

NavBar.defaultProps = {
    backButton: true,
};

export default NavBar;

