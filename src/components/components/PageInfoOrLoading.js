import React from "react";

const PageInfoOrLoading = (props) => (
    <div className="page-info">
        {
            !props.children &&
            <>
                <div className="fa-2x">
                    <i className="fas fa-spinner fa-pulse"  />
                </div>
                <br />
                Loading...
            </>
        }
        {props.children && props.children}
    </div>
);

export default PageInfoOrLoading;
