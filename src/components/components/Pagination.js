import React from "react";
import PropTypes from "prop-types"


const Pagination = (props) => (
    <div className="pagination">
        <div style={{display: "flex"}}>
            {/* Only show "To Start" button if we aren't on the first set of results */}
            {
                props.offset !== 0 &&
                <button className="button button--icon" onClick={() => props.updateOffset(0)} title="To Start">
                    <i className="fas fa-step-backward"/>
                </button>
            }

            {/* Disable button if we are on the first set of results */}
            <button
                className="button"
                disabled={props.offset === 0}
                onClick={() => props.updateOffset((props.offset - props.limit) > 0 ? props.offset - props.limit : 0)}
            >Previous
            </button>
        </div>

        <span className="pagination__info">
            Results <strong>{props.offset + 1}</strong> to&nbsp;
            <strong>{(props.offset + props.limit) > props.numberOfResults ? props.numberOfResults : props.offset + props.limit}</strong> out of&nbsp;
            <strong>{props.numberOfResults}</strong>
        </span>

        {/* Disable button if we are on the last set of results */}
        <button
            className="button"
            disabled={props.offset + 1 >= props.numberOfResults}
            onClick={() => props.updateOffset(props.offset + props.limit)}
        >Next
        </button>
    </div>
);

Pagination.propTypes = {
    limit: PropTypes.number,
    offset: PropTypes.number,
    numberOfResults: PropTypes.number,
    updateOffset: PropTypes.func,
};

export default Pagination;
